import cv2
import csv
import pandas as pd
import pytesseract

# reading and processing the image for better accuracy
img = cv2.imread('image.png')
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
# img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
img = cv2.resize(img, None, fx=2, fy=2, interpolation=cv2.INTER_CUBIC)

# kernel = np.ones((1, 1), np.uint8)
# img = cv2.dilate(img, kernel, iterations=1)
# img = cv2.erode(img, kernel, iterations=1)
pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'
text = pytesseract.image_to_string(img)
text = text.strip()

myText = open('imgText.txt', 'w')
myText.write(text)
myText.close()

f = open('imgText.txt')
lines = f.readlines()
total_lines = len(lines)
# print("Total Lines : ", total_lines)

# Just for line 21 to 24
start = 36  # int(input('starting line number: '))
end = 40  # int(input('ending line number: '))
final_list = []
for i in range(start, end, 1):
    strings = lines[i].split()
    new_list = []
    name_list = ''
    flag = 0

    for i in strings:
        if i.endswith(","):
            i = i.replace(",", "")
        if not i.isalpha() | i.endswith(".") | i.endswith(")"):  # if not a name
            if flag == 1:
                new_list.append(name_list)
                new_list.append(i)
                flag = 0
            else:
                new_list.append(i)
        else:
            flag = 1
            name_list = name_list + i + " "
    final_list.append(new_list)
with open('csvFile.csv', 'w') as f:
    write = csv.writer(f)
    write.writerow(["No.", "Code", "Service Name (Notes)", "Rate", "Qty.", "Amount", "Disc.", "Net Amt."])
    write.writerows(final_list)

df = pd.read_csv('csvFile.csv', index_col='No.')
print(df)
