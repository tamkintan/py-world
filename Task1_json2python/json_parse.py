import json

with open('json_input.json') as f:
    data = json.load(f)
print(data)

#In JSON, values must be a string/number/JSON object/Array/Boolean/Null. So, I have printed the type of data too.
print("\nName: ",data['name'], type(data['name']))
print("\nAge: ",data['age'], type(data['age']))
print("\nAddress: ", type(data['address']))
print("House#: ", data['address']['house'])
print("Street#: ", data['address']['street'])
print("City: ", data['address']['city'])
print("\nChildren: ", data['children'], type(data['children']))
print("\nCorona Infected: ",data['corona_infected'], type(data['corona_infected']))
print("\nInfection Period: ",data['infection_period'], type(data['infection_period']))
